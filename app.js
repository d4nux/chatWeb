var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var path = require('path');

app.use(express.static(path.join(__dirname, 'public')));

server.listen(3000);
console.log('Server running at http://127.0.0.1:3000/');

app.get('/', function(req, res){
	res.sendfile(__dirname + '/index.html')
});


io.sockets.on('connection', function(socket){
	socket.on('send message', function(data){
			io.sockets.emit('new message', data);
	});
});
